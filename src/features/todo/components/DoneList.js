
import DoneGroup from "./DoneGroup";
import React from "react";

export default function DoneList() {
    return(
        <row gutter={{
            xs: 8,
            sm: 16,
            md: 24,
            lg: 32,
        }}>
        <div className='todo-list'>
            <h1>Done List</h1>
            <DoneGroup />
        </div>
        </row>
    );
}