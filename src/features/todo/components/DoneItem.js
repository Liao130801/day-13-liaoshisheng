import { useDispatch } from "react-redux";
import {useNavigate} from "react-router";
import {useTodos} from "../../hooks/useTodo";

export default function DoneItem(props) {
    const navigate = useNavigate()
    const {deleteTodo} = useTodos()
    const handleTaskNameClick = () => {
        navigate('/done/'+props.task.id)
    }

    const hanlleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(props.task.id)
        }
    }
    return(
        <div className='todo-item' onClick={handleTaskNameClick}>
            <div className={`task-name ${ props.task.done ? 'done' : ''}`}>{props.task.name}</div>
            <div className='remove-button'  onClick={hanlleRemoveButtonClick}>x</div>
        </div>
    );
}