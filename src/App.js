import './App.css';
import {NavLink,Outlet} from "react-router-dom";
import {Menu} from "antd";
import React from "react";

function App() {
  return (
      <div className="app">
          <Menu mode="horizontal" style={{ width: '250px' }}>
              <Menu.Item key="home">
                  <NavLink to={'/'}>Home</NavLink>
              </Menu.Item>
              <Menu.Item key="done">
                  <NavLink to={'/done'}>Done List</NavLink>
              </Menu.Item>
              <Menu.Item key="help">
                  <NavLink to={'/help'}>Help</NavLink>
              </Menu.Item>
          </Menu>

                  <Outlet />

      </div>
  );
}
export default App;
