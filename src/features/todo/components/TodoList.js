import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import React, {useEffect} from "react";
import {useTodos} from "../../hooks/useTodo";

export default function TodoList() {
    const {loadTodos} = useTodos()
    useEffect(()=>{
        loadTodos()
    },[])
    return(
        <row gutter={{
            xs: 8,
            sm: 16,
            md: 24,
            lg: 32,
        }}>
        <div className='todo-list'>
            <h1>Todo List</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
        </row>
    );
}