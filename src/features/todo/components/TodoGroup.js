import TodoItem from "./TodoItem";
import {useSelector} from 'react-redux'
import {Row,Col} from 'antd';
import React from "react";

export default function TodoGroup() {
    const todoTasks = useSelector(state => state.todo.tasks)
    return (

        <Row gutter={{xs: 8, sm: 16, md: 24}} className='todo-group'>


            {todoTasks.map(((todoTask) =>
                    <Col sm={12} md={8} lg={6} xl={6}>
                        <TodoItem key={todoTask.id} task={todoTask}></TodoItem>
                    </Col>
            ))}
        </Row>
    );
}