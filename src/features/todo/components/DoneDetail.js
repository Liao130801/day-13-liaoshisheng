import React from "react";
import {useParams} from "react-router";
import {useSelector} from "react-redux";

export default function DoneDetail (){
    const {id} = useParams()
    const todoTask = useSelector(state => state.todo.tasks.find(task=>task.id===id))
    return (
        <div className="doneDetail">
            <div>{todoTask?.name}</div>
        </div>
    )
}