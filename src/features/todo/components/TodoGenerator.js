import { useState } from "react";
import {useTodos} from "../../hooks/useTodo";
import {v4 as uuidv4} from "uuid";
import {Input,Button} from "antd";

export default function TodoGenerator() {
    const [taskName, setTaskName] = useState("");
    const {createTodo} = useTodos()
    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleAddTodoTask=async () => {
       await createTodo({id: uuidv4(), name:taskName, done: false})
        setTaskName("");
    }
    return(
        <div className='todo-generator'>
            <Input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></Input>
            <Button onClick={handleAddTodoTask} disabled={ !taskName } > Add </Button>
        </div>
    );
}