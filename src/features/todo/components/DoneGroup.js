
import { useSelector } from 'react-redux'
import DoneItem from "./DoneItem";

export default function DoneGroup() {
    const todoTasks = useSelector(state => state.todo.tasks.filter(task=>task.done))
    return (
        <div className='todo-group'>
            {todoTasks.map(((todoTask) =>
                    <DoneItem key= {todoTask.id} task={todoTask}></DoneItem>
            ))}
        </div>
    );
}