###1.Key learning, skills and experience acquired：
- 1). Today the first code review, by refactoring classmates code to standardise their own code, and at the same time can learn from classmates excellent ideas, today I see classmates use excellent code writing, code is very concise, I want to learn more.
- 2). Axios and promise, which is the main form of interaction between front-end and back-end. The main problem encountered is not familiar with the asynchronous call, often some errors, the next step is to learn more about this call, compare the difference between synchronous and asynchronous, in the right place to use the right call!
- 3). In the afternoon, I learnt about ant design, which is an excellent component library that allows you to form better looking pages by combining components. This is the same component library that I used during my internship, and I'm quite familiar with it, so my goal is to go deeper to understand the principles of how it works, and to ensure that there are no errors while building pages more efficiently.
###2.Problem / Confusing / Difficulties
- I still need to learn more.
###3.Other Comments / Suggestion
- Nothing
