import {useTodos} from "../../hooks/useTodo";
import {CloseOutlined, EditOutlined} from "@ant-design/icons";
import  {Row,Modal} from 'antd';
import React, {useState} from "react";

export default function TodoItem(props) {
    const {confirm} = Modal;
    const {updateTodo, deleteTodo} = useTodos()
    const [taskName, setTaskName] = useState(props.task.name);
    const handleTaskNameClick = async () => {
        await updateTodo(props.task.id, {done: !props.task.done})
    }

    const hanlleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(props.task.id)
        }
    }
    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }
    const editTaskNameClick = () => {
        confirm({
            title: 'Do you Want to edit these items?',
            content: <input onChange={handleTaskNameChange}/>,
            onOk() {
                setTaskName(taskName => updateTodo(props.task.id, {name: taskName}))
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }
    return (

            <div  className='todo-item'>
                <div className={`task-name ${props.task.done ? 'done' : ''}`}
                     onClick={handleTaskNameClick}>{props.task.name}</div>
                <div className='remove-button'>
                    <CloseOutlined onClick={hanlleRemoveButtonClick}/>
                    <EditOutlined onClick={editTaskNameClick}/>
                </div>
            </div>
    );
}
