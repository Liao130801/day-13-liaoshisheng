import {createTodoTask, deleteTodoTask, getTodoTasks, updateTodoTask} from "../../api/todo";
import {initTodoTask} from "../todo/reducers/todoSlice";
import {useDispatch} from "react-redux";


export const useTodos = ()=>{
    const dispatch = useDispatch();
    async function loadTodos(){
        const res = await getTodoTasks()
        dispatch(initTodoTask(res.data))
    }
    const updateTodo = async (id,todoTsak)=>{
        await updateTodoTask(id,todoTsak)
        loadTodos()
    }
    const deleteTodo = async (id) =>{
        await deleteTodoTask(id)
        loadTodos()
    }
    const createTodo = async (todoTask) =>{
        await createTodoTask(todoTask)
        loadTodos()
    }
    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        createTodo
    }
}